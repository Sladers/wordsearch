# WordSearch

Simple Word Search generator and player made with JavaFX.  Created to hopefully help my son with his spelling homework.

To create your own wordsearch create a text file with each one on its own line.  See the download's page for a build and sample word lists.