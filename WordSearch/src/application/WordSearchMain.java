package application;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WordSearchMain extends Application {

	@Override
	public void start(Stage stage) {
		System.out.println("Start");
		try {
			URL url = this.getClass().getResource("WordSearch.fxml");
			System.out.println("FXML url: " + url);
			FXMLLoader loader = new FXMLLoader(url);

			Parent p = loader.load();
			WordSearchController controller = loader.getController();
			String[] words = { "little", "work", "know", "Place", "years", "rain", "mail", "wait", "paint", "chant",
					"paid", "sail", "goods", "services", "consumer", "producer" };

			controller.setWords(words);

			Scene scene = new Scene(p, 500, 500);
			stage.setScene(scene);
			stage.show();
			controller.setCanvas();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
