package application;

import java.awt.Desktop;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

public class WordSearchController {

	/**
	 * Inserts the given word into the grid. Does not check bounds or if it
	 * overwrites part of another word. It is assumed that has been checked
	 * already.
	 *
	 * @param word
	 * @param grid
	 */
	private static void insert(WordSearchWord word, char[][] grid) {
		System.out
				.println("Inserting \"" + word.word + "\" at " + word.getPosition() + " going " + word.getDirection());
		char[] wordArray = word.word.toCharArray();
		Point position = word.getPosition();
		Direction direction = word.getDirection();
		for (int i = 0; i < wordArray.length; i++) {
			grid[position.x + i * direction.x][position.y + i * direction.y] = wordArray[i];
		}
	}

	/**
	 * Removes the given word from the grid. Possibly removes a letter from an
	 * intersecting word already present in the grid
	 * 
	 * @param wsword
	 * @param grid
	 */
	private static void remove(WordSearchWord wsword, char[][] grid) {
		char[] wordArray = wsword.word.toCharArray();
		Point position = wsword.getPosition();
		Direction direction = wsword.getDirection();
		for (int i = 0; i < wordArray.length; i++) {
			grid[position.x + i * direction.x][position.y + i * direction.y] = (char) 0;
		}
	}

	/**
	 * Checks if the given word can fit at the given position and direciton in
	 * the given grid
	 * 
	 * @param grid
	 * @param word
	 * @param position
	 * @param direction
	 * @return if it fits or not
	 */
	private static boolean testWord(char[][] grid, String word, Point position, Direction direction) {

		// check bounds
		int xEnd = position.x + (direction.x * word.length() - 1);
		int yEnd = position.y + (direction.y * word.length() - 1);

		if (position.x >= 0 && position.x < grid.length //
				&& position.y >= 0 && position.y < grid[0].length//
				&& xEnd >= 0 && xEnd < grid.length //
				&& yEnd >= 0 && yEnd < grid[0].length//
		) {
			char[] wordArray = word.toCharArray();
			// check if each position is either empty or the same
			for (int i = 0; i < wordArray.length; i++) {
				int x = position.x + i * direction.x;
				int y = position.y + i * direction.y;
				char c = grid[x][y];
				if (c != 0 && c != wordArray[i]) {
					return false;
				}
			}
			return true;
		}

		return false;
	}

	@FXML
	private VBox wordVBox;

	private GridPane searchGrid = new GridPane();

	private Label[][] letters = null;
	@FXML
	private StackPane stackPane;

	private List<WordSearchWord> words = null;

	private Pane correct = new Pane();
	private Pane incorrect = new Pane();

	private Line line = new Line();

	private int rows;
	private int columns;

	private int rowHeight = 0;
	private int columnWidth = 0;

	private final Random rand = new Random();

	//open a link to the github page for this project
	@FXML
	private void about() {
		try {
			Desktop.getDesktop().browse(new URI("https://bitbucket.org/Sladers/wordsearch/src/master/"));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns a list of all possible directions in a random order
	 * 
	 * @return
	 */
	private List<Direction> directions() {
		List<Direction> directions = Arrays.asList(Direction.values());

		Collections.shuffle(directions, rand);

		return directions;
	}

	/**
	 * Convenience method for setting up panes in the stack pane so that they
	 * are the right size and passthrough mouse actions
	 * 
	 * @param pane
	 */
	private void init(Pane pane) {
		pane.minHeightProperty().bind(searchGrid.heightProperty());
		pane.minWidthProperty().bind(searchGrid.widthProperty());
		pane.setMouseTransparent(true);
	}

	@FXML
	public void initialize() {
		System.out.println("Initialize");
		searchGrid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		searchGrid.setHgap(0);
		searchGrid.setVgap(0);
		setMouseListener(searchGrid);

		Pane group = new Pane();
		group.getChildren().add(line);
		init(group);
		init(correct);
		init(incorrect);

		stackPane.getChildren().addAll(searchGrid, /* canvas, */ group, correct, incorrect);
	}

	/**
	 * Attempts to match a word denoted by the line going from *Start to *End
	 * indexes of the searchGrid
	 * 
	 * @param columnStart
	 * @param rowStart
	 * @param columnEnd
	 * @param rowEnd
	 */
	private void matchWord(int columnStart, int rowStart, int columnEnd, int rowEnd) {

		// ensure that the line is legal (straight across or a 45 degree angle)
		int dy = rowEnd - rowStart;
		int dx = columnEnd - columnStart;
		if (dx == 0 || dy == 0 || Math.abs(dx) == Math.abs(dy)) {
			// we have legal start and end points

			int length = Math.abs(dx);
			if (Math.abs(dy) > length) {
				length = Math.abs(dy);
			}
			length = length + 1;

			// create a unit vector in the direction of x or -x
			int unitX = dx;
			if (unitX != 0) {
				unitX = unitX / Math.abs(unitX);
			}
			// create a unit vector in the direciton of y or -y
			int unitY = dy;
			if (unitY != 0) {
				unitY = unitY / Math.abs(unitY);
			}

			// build a char array of the line
			char[] carray = new char[length];
			for (int i = 0; i < length; i++) {
				int x = columnStart + i * unitX;
				int y = rowStart + i * unitY;
				carray[i] = letters[x][y].getText().charAt(0);
			}

			// check if the selected string is a word in our list
			String selected = new String(carray);
			System.out.println("Selected: " + selected);

			WordSearchWord match = null;
			for (WordSearchWord wsword : words) {
				// we check if the text is strikethrough to see if this word has
				// been found already
				if (wsword.word.equalsIgnoreCase(selected) && !wsword.getLabel().isStrikethrough()) {
					match = wsword;
					break;
				}
			}
			if (match == null) {
				// check if the reverse is a word in our word list
				char[] backwards = new char[carray.length];
				for (int i = 0; i < backwards.length; i++) {
					backwards[i] = carray[carray.length - i - 1];
				}

				String selectedBackwards = new String(backwards);
				System.out.println("Backwards: " + selectedBackwards);

				for (WordSearchWord wsword : words) {
					if (wsword.word.equalsIgnoreCase(selectedBackwards) && !wsword.getLabel().isStrikethrough()) {
						match = wsword;
						break;
					}
				}
			}
			System.out.println("Matched: " + match);

			// draw a line where the user selected, centered on the grid cells
			int xStart = columnStart * columnWidth + columnWidth / 2;
			int yStart = rowStart * rowHeight + rowHeight / 2;
			int xEnd = columnEnd * columnWidth + columnWidth / 2;
			int yEnd = rowEnd * rowHeight + rowHeight / 2;

			Line line = new Line(xStart, yStart, xEnd, yEnd);
			line.setStrokeWidth(5);
			line.setStrokeLineCap(StrokeLineCap.ROUND);
			line.setOpacity(0.5);

			if (match != null) {
				// Add a green line to the correct pane.
				line.setStroke(Color.GREEN);
				correct.getChildren().add(line);
				match.getLabel().setStrikethrough(true);
				match.getLabel().setStyle("-fx-font-weight: bold");
			} else {
				// add a red line to the incorrect pane
				line.setStroke(Color.RED);
				incorrect.getChildren().add(line);
			}
		} else {
			// stand and end points are on a strange angle
		}
	}

	private void mouseDragged(MouseEvent event) {
		// System.out.println("Mouse Dragged: " + event.getX() + ", " +
		// event.getY());

		setLineEnd(event);
	}

	private void mousePressed(MouseEvent event) {
		System.out.println("Mouse Pressed: " + event.getX() + ", " + event.getY() + ", source: " + event.getSource());

		line.setStartX(event.getX());
		line.setStartY(event.getY());
		setLineEnd(event);

		// remove the last incorrect line
		incorrect.getChildren().clear();
	}

	private void mouseReleased(MouseEvent event) {
		System.out.println("Mouse Released: " + event.getSceneX() + ", " + event.getSceneY());
		// setLineEnd(event);

		// find the start and end letter of the line drawn

		int rowStart = (int) (line.getStartY() / rowHeight);
		int rowEnd = (int) (event.getY() / rowHeight);
		int columnStart = (int) (line.getStartX() / columnWidth);
		int columnEnd = (int) (event.getX() / columnWidth);

		System.out.println("Rows: " + rows + ", columns: " + columns);
		System.out.println("rowHeight: " + rowHeight + ", column width: " + columnWidth);
		System.out.println("Selected: " + columnEnd + ", " + rowEnd);

		line.setStartX(0);
		line.setStartY(0);
		line.setEndX(0);
		line.setEndY(0);

		matchWord(columnStart, rowStart, columnEnd, rowEnd);
	}

	@FXML
	private void open() {
		FileChooser chooser = new FileChooser();
		File chosen = chooser.showOpenDialog(wordVBox.getScene().getWindow());
		if (chosen != null) {
			try (Scanner scan = new Scanner(chosen)) {
				List<String> list = new ArrayList<>();
				String line;
				while (scan.hasNextLine()) {
					line = scan.nextLine();
					if (line != null && !line.isEmpty()) {
						list.add(line);
					}
				}
				setWords(list.toArray(new String[list.size()]));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	private List<Point> positions(int x, int y) {
		List<Point> positions = new ArrayList<>();
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				positions.add(new Point(i, j));
			}
		}
		Collections.shuffle(positions, rand);
		return positions;
	}

	/**
	 * Closes the program if the user confirms
	 */
	@FXML
	private void quit() {
		Alert confirmation = new Alert(AlertType.CONFIRMATION);
		confirmation.setContentText("Are you sure you want to quit?");
		confirmation.initOwner(wordVBox.getScene().getWindow());
		if (confirmation.showAndWait().get() == ButtonType.OK) {
			Platform.exit();
		}
	}

	/**
	 * Shuffles the wordsearch grid and resets all found words, if the user
	 * confirms
	 */
	@FXML
	private void restart() {
		Alert confirmation = new Alert(AlertType.CONFIRMATION);
		confirmation.setContentText("Are you sure you want to restart?");
		confirmation.initOwner(wordVBox.getScene().getWindow());
		if (confirmation.showAndWait().get() == ButtonType.OK) {
			String[] words = new String[this.words.size()];
			for (int i = 0; i < words.length; i++) {
				words[i] = this.words.get(i).word;
			}
			setWords(words);
		}
	}

	public void setCanvas() {
		rowHeight = (int) (searchGrid.getHeight() / rows);
		columnWidth = (int) (searchGrid.getWidth() / columns);
	}

	private void setLineEnd(MouseEvent event) {
		line.setEndX(event.getX());
		line.setEndY(event.getY());
	}

	private void setMouseListener(Node n) {
		n.setOnMousePressed(event -> mousePressed(event));
		n.setOnMouseDragged(event -> mouseDragged(event));
		n.setOnMouseReleased(event -> mouseReleased(event));
	}

	public void setWords(String[] words) {
		int size = 12;
		for (String s : words) {
			if (s.length() > size) {
				size = s.length();
			}
		}
		setWords(words, size, size);
	}

	public void setWords(String[] wwords, int x, int y) {
		this.columns = x;
		this.rows = y;

		// reset the search grid
		wordVBox.getChildren().clear();
		searchGrid.getChildren().clear();
		words = new ArrayList<>(wwords.length);
		correct.getChildren().clear();
		incorrect.getChildren().clear();

		Insets insets = new Insets(4);
		for (String s : wwords) {
			WordSearchWord wsword = new WordSearchWord(s);
			words.add(wsword);

			Text label = new Text(wsword.word);
			wordVBox.getChildren().add(label);
			VBox.setMargin(label, insets);
			wsword.setLabel(label);
		}

		char[][] grid = new char[x][y];

		// keep track of whether or not we have fit the word into the grid each
		// loop
		boolean fitWord = false;

		//
		Map<String, Integer> failMap = new HashMap<>();
		for (int wordIndex = 0; wordIndex < words.size(); wordIndex++) {
			WordSearchWord wsword = words.get(wordIndex);
			System.out.println("Looking at word: \"" + wsword.word + "\"");
			fitWord = false;
			List<Point> positions = positions(x, y);
			for (int positionIndex = 0; positionIndex < positions.size() && !fitWord; positionIndex++) {
				Point position = positions.get(positionIndex);
				List<Direction> directions = directions();
				for (int directionIndex = 0; directionIndex < directions.size() && !fitWord; directionIndex++) {
					Direction direction = directions.get(directionIndex);

					if (testWord(grid, wsword.word, position, direction)) {
						// fill the word into the grid at the given point and
						// direction

						wsword.setPosition(position);
						wsword.setDirection(direction);
						insert(wsword, grid);
						fitWord = true;
					}
				}
			}

			if (!fitWord) {
				if (!failMap.containsKey(wsword.word)) {
					failMap.put(wsword.word, 0);
				}
				int fails = failMap.get(wsword.word);
				fails++;
				failMap.put(wsword.word, fails);
				System.out.println("Failed to insert \"" + wsword.word + "\"" + fails + " times, backing up");
				// can't fit this word anywhere, need to back up

				for (int i = 0; i < fails; i++) {
					WordSearchWord wordToRemove = words.get(wordIndex - 1 - i);
					System.out.println("Removing \"" + wordToRemove.word + "\"");
					remove(wordToRemove, grid);
					wordToRemove.setDirection(null);
					wordToRemove.setPosition(null);
				}
				// reinsert existing words in case an intersection was removed
				// inserting every word is easier than looking for intersections
				for (WordSearchWord oldWord : words) {
					if (oldWord.getPosition() != null) {
						insert(oldWord, grid);
					}
				}
				// backup to previous word. Subtract by two because counter will
				// increment at end of loop.
				wordIndex -= (fails + 1);
			}

		}

		// fill the rest of the grid with random letters
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if (grid[i][j] == 0) {
					grid[i][j] = (char) ('A' + rand.nextInt(26));
				}
			}
		}

		// fill the searchGrid with labels
		insets = new Insets(2);
		letters = new Label[x][y];
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				Label label = new Label(Character.toString(grid[i][j]));
				label.setMinSize(20, 20);
				label.setStyle("-fx-border-color: black;");
				label.setAlignment(Pos.CENTER);
				// GridPane.setMargin(label, insets);
				label.setPadding(insets);
				searchGrid.add(label, i, j);
				letters[i][j] = label;
			}
		}

	}
}
