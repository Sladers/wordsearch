package application;

import java.awt.Point;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.text.Text;

/**
 * Word belonging to a WordSearch. Great class name
 */
public class WordSearchWord {

	private BooleanProperty foundProperty = new SimpleBooleanProperty();

	public void setFound(boolean found) {
		this.foundProperty.set(found);
	}

	public boolean getFound() {
		return foundProperty.get();
	}

	public BooleanProperty foundProperty() {
		return foundProperty;
	}

	private Text label;

	public Text getLabel() {
		return label;
	}

	public void setLabel(Text label) {
		this.label = label;
	}

	public final String word;

	public WordSearchWord(String word) {
		this.word = word.toUpperCase();
	}

	private Direction direction;
	private Point position;

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public String getWord() {
		return word;
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof WordSearchWord && word.equalsIgnoreCase(((WordSearchWord) o).word);
	}

	@Override
	public int hashCode() {
		return word.hashCode();
	}

	@Override
	public String toString() {
		return word;
	}

}
